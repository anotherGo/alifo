package alifo

import "unsafe"

type Item struct {
	next unsafe.Pointer
	Data interface{}
}

func (i *Item) Next() *Item {
	return (*Item)(i.next)
}
func (i *Item) SkipNext() {
	i.next = i.Next().next
}
func (i *Item) SkipAllNexts() {
	i.next = nil
}

// Push adds a new item to end of List
