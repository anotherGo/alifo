package main

import (
	"fmt"
	"time"

	"gitea.com/anotherGo/alifo"
)

func main() {
	lifo := &alifo.LIFO{}
	for i := 0; i < 300; i++ {
		d := i
		go func() {
			lifo.Push(d)

		}()
	}
	<-time.NewTimer(1 * time.Second).C
	i := 0
	for it := lifo.Head(); it != nil; it = it.Next() {
		//fmt.Println(it.Data)
	}
	fmt.Println(i, lifo.Count())
}
